//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Worknrby_WebAPI.App_Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Employee
    {
        public Employee()
        {
            this.EmployeeSkills = new HashSet<EmployeeSkill>();
            this.EmployeeViews = new HashSet<EmployeeView>();
            this.EmployeeWishlists = new HashSet<EmployeeWishlist>();
            this.JobApplications = new HashSet<JobApplication>();
            this.JobOpeningViews = new HashSet<JobOpeningView>();
            this.JobOpeningWishlists = new HashSet<JobOpeningWishlist>();
            this.SpamReports = new HashSet<SpamReport>();
            this.SpamReports1 = new HashSet<SpamReport>();
        }
    
        public long ID { get; set; }
        public long UserID { get; set; }
        public string Name { get; set; }
        public long GenderID { get; set; }
        public string DOB { get; set; }
        public string Mobile { get; set; }
        public string MobileVerificationCode { get; set; }
        public bool MobileVerified { get; set; }
        public long LocID { get; set; }
        public string Address { get; set; }
        public long JobTypeID { get; set; }
        public decimal YrsOfExperience { get; set; }
        public long EduLevelID { get; set; }
        public string Qualification { get; set; }
        public bool IsNotLookingForJob { get; set; }
        public decimal ExpectedSalaryMonthly { get; set; }
        public bool IsNegotiable { get; set; }
        public int RadiusInKM { get; set; }
        public string Remarks { get; set; }
        public string CBOExpression { get; set; }
        public bool IsActive { get; set; }
        public int CreatedByUserID { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedByUserID { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public int OrgID { get; set; }
    
        public virtual EduLevel EduLevel { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual JobType JobType { get; set; }
        public virtual Loc Loc { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<EmployeeSkill> EmployeeSkills { get; set; }
        public virtual ICollection<EmployeeView> EmployeeViews { get; set; }
        public virtual ICollection<EmployeeWishlist> EmployeeWishlists { get; set; }
        public virtual ICollection<JobApplication> JobApplications { get; set; }
        public virtual ICollection<JobOpeningView> JobOpeningViews { get; set; }
        public virtual ICollection<JobOpeningWishlist> JobOpeningWishlists { get; set; }
        public virtual ICollection<SpamReport> SpamReports { get; set; }
        public virtual ICollection<SpamReport> SpamReports1 { get; set; }
    }
}

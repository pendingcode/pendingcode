﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Worknrby_WebAPI;
using Worknrby_WebAPI.App_Data;

static class PushNotifications
{
    public static void PushNotificationOnEmployerApply(WnbNewEntities _context, int EmployeeID, int JobApplicationDirectionID)
    {
        if (JobApplicationDirectionID == 6)
            return;
        var employee = _context.Employees.FirstOrDefault(m => m.ID == EmployeeID);
        var UserID = employee.UserID;
        PushNotification objPN = new PushNotification();
        objPN.UserID = UserID;
        if (JobApplicationDirectionID == 2)//Accept/Shortlisted- Shortlisted From Employer
        {
            objPN.Name = Global.PushMsg_FromEmployerOnShortlist;
        }
        else if (JobApplicationDirectionID == 4)//Accepted/Accepted- Accepted From Employer
        {
            objPN.Name = Global.PushMsg_FromEmployerOnAccept;
        }
        Global.ErrorFreeObject(objPN);
        _context.PushNotifications.Add(objPN);
        _context.SaveChanges();

        return;
        var gcmUsers = (from g in _context.GcmUsers
                        join e in _context.Employees on g.UserID equals e.UserID
                        where e.ID == EmployeeID
                        select g).ToList();
        if (gcmUsers.Count > 0)
        {
            foreach (var gcmUser in gcmUsers)
            {
                if (JobApplicationDirectionID == 2)//Accept/Shortlisted- Shortlisted From Employer
                {
                    AndroidGCMPushNotification.SendNotification(gcmUser.RegIDNo, Global.PushMsg_FromEmployerOnShortlist);
                }
                else if (JobApplicationDirectionID == 4)//Accepted/Accepted- Accepted From Employer
                {
                    AndroidGCMPushNotification.SendNotification(gcmUser.RegIDNo, Global.PushMsg_FromEmployerOnAccept);
                }
            }
        }
    }
    
    public static void PushNotificationOnJobseekerApply(WnbNewEntities _context, int JobOpeningID, int JobApplicationDirectionID)
    {
        if (JobApplicationDirectionID == 5)
            return;
        var user = (from j in _context.JobOpenings
                    join eb in _context.EmployerBranches on j.EmployerBranchID equals eb.ID
                    join e in _context.Employers on eb.EmployerID equals e.ID
                    select e).FirstOrDefault();

        var UserID = user.UserID;
        PushNotification objPN = new PushNotification();
        objPN.UserID = UserID;
        if (JobApplicationDirectionID == 1)//Accept/Shortlisted- Shortlisted From Employer
        {
            objPN.Name = Global.PushMsg_FromJobseekerOnApply;
        }
        else if (JobApplicationDirectionID == 3)//Accepted/Accepted- Accepted From Employer
        {
            objPN.Name = Global.PushMsg_FromJobseekerOnAccept;
        }
        Global.ErrorFreeObject(objPN);
        _context.PushNotifications.Add(objPN);
        _context.SaveChanges();
    }

    public static void PushNotificationOnJobOpening(WnbNewEntities _context, int JobOpeningID)
    {
        
    }

    public static void PushNotificationOnCreateCV(WnbNewEntities _context, int EmployeeID)
    {

    }
}

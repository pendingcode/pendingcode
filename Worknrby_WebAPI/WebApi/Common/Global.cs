﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Worknrby_WebAPI.App_Data;

namespace Worknrby_WebAPI
{
    public class Global
    {
        public enum OrderStatus
        {
            Pending = 1,
            Success = 2,
            Fail = 3
        }

        public static int CreditDecutOnEmployeeRequest = 1;
        public static int CreditDecutOnEmployerRequest = 2;
        public static int RadiusInKM = 5;

        public static string PushMsg_FromEmployerOnShortlist = "PushMsg_FromEmployerOnShortlist";
        public static string PushMsg_FromEmployerOnAccept = "PushMsg_FromEmployerOnAccept";
        public static string PushMsg_FromJobseekerOnApply = "PushMsg_FromJobseekerOnApply";
        public static string PushMsg_FromJobseekerOnAccept = "PushMsg_FromJobseekerOnAccept";

        #region "APPLICATION SMS"
        public static bool SendMessage(string SendTo, string Message)
        {
            bool Status = false;
            WebClient wc = new WebClient();
            string Response = wc.DownloadString(string.Format("http://alerts.sinfini.com/api/web2sms.php?workingkey=A76d45cbe5d6eaf628de355003ab18646&to={0}&sender=EHCARE&message={1}", SendTo, WebUtility.UrlEncode(Message)));
            if (Response.Contains("Message GID"))
                Status = true;
            else
                Status = false;
            return Status;
        }
        #endregion

        public static void ErrorFreeObject<TEnum>(TEnum t) where TEnum : class
        {
            try
            {
                foreach (PropertyDescriptor propertyInfo in TypeDescriptor.GetProperties(typeof(TEnum)))
                {
                    if (propertyInfo.PropertyType == typeof(string))
                    {
                        if (propertyInfo.GetValue(t) == null)
                            propertyInfo.SetValue(t, "");
                    }
                    else if (propertyInfo.PropertyType == typeof(int))
                    {
                        if (propertyInfo.GetValue(t) == null || (int)propertyInfo.GetValue(t) == 0)
                            propertyInfo.SetValue(t, -1);
                    }
                    else if (propertyInfo.PropertyType == typeof(long))
                    {
                        if (propertyInfo.GetValue(t) == null || (long)propertyInfo.GetValue(t) == 0)
                            propertyInfo.SetValue(t, -1);
                    }
                    else if (propertyInfo.PropertyType == typeof(DateTime))
                    {
                        if (propertyInfo.GetValue(t) == null || (DateTime)propertyInfo.GetValue(t) == DateTime.MinValue)
                            propertyInfo.SetValue(t, DateTime.Now);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static string GetAccessToken(int UserID, int ID, int JobID = 0)
        {
            return UserID + "_" + ID + "_" + JobID;
        }

        public static int GetUserID(WnbNewEntities _context, string AccessToken)
        {
            int UserID = 0;
            if (!string.IsNullOrEmpty(AccessToken))
            {
                string strUser = AccessToken.Split('_')[0];
                int.TryParse(strUser, out UserID);
            }
            return UserID;
        }

        public static int GetEmployeeID(WnbNewEntities _context, string AccessToken)
        {
            int EmployeeID = 0;
            if (!string.IsNullOrEmpty(AccessToken))
            {
                if (AccessToken.Contains('_'))
                {
                    var lst = AccessToken.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    if (lst.Count > 1)
                    {
                        int.TryParse(lst[1], out EmployeeID);
                    }
                }
                if (EmployeeID == 0)
                {
                    var UserID = GetUserID(_context, AccessToken);
                    if (UserID != 0)
                    {
                        var employee = _context.Employees.FirstOrDefault(m => m.UserID == UserID);
                        if (employee != null)
                            return (int)employee.ID;
                    }
                }

            }
            return EmployeeID;
        }

        public static int GetEmployerID(WnbNewEntities _context, string AccessToken)
        {
            int EmployerID = 0;
            if (!string.IsNullOrEmpty(AccessToken))
            {
                if (AccessToken.Contains('_'))
                {
                    var lst = AccessToken.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    if (lst.Count > 1)
                    {
                        int.TryParse(lst[1], out EmployerID);
                    }
                }
                if (EmployerID == 0)
                {
                    var UserID = GetUserID(_context, AccessToken);
                    if (UserID != 0)
                    {
                        var employer = _context.Employers.FirstOrDefault(m => m.UserID == UserID);
                        if (employer != null)
                            return (int)employer.ID;
                    }
                }

            }
            return EmployerID;
        }

        public static int GetJobOpeningID(WnbNewEntities _context, string AccessToken)
        {
            int JobOpeningID = 0;
            if (!string.IsNullOrEmpty(AccessToken))
            {
                if (AccessToken.Contains('_'))
                {
                    var lst = AccessToken.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    if (lst.Count > 2)
                    {
                        int.TryParse(lst[2], out JobOpeningID);
                    }
                }
                if (JobOpeningID == 0)
                {
                    var EmployerID = GetEmployerID(_context, AccessToken);
                    if (EmployerID != 0)
                    {
                        var jobOpening = _context.JobOpenings.FirstOrDefault(m => m.EmployerBranch.EmployerID == EmployerID);
                        if (jobOpening != null)
                            return (int)jobOpening.ID;
                    }
                }

            }
            return JobOpeningID;
        }

        public static double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return (TimeZoneInfo.ConvertTimeToUtc(dateTime) -
                   new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class UserProfile
{
    public string Email { get; set; }

    public string Name { get; set; }

    public string DOB { get; set; }

    public string Mobile { get; set; }

    public int RadiusInKM { get; set; }

    public long LocID { get; set; }

    public string PinCode { get; set; }

    public string Location { get; set; }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Worknrby_WebAPI
{
    public class TextValue
    {
        public long Id { get; set; }

        public string Value { get; set; }

        public long ParentId { get; set; }
    }
}

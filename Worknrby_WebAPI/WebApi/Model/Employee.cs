﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Worknrby_WebAPI.App_Data
{
    public partial class Employee
    {
        public string Skills { get; set; }
        public string SkillRatings { get; set; }
    }
}

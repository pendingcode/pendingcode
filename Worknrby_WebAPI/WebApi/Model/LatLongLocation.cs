﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Worknrby_WebAPI
{
    class LatLongLocation
    {
        public long id { get; set; }

        public string name { get; set; }

        public decimal? Lat { get; set; }

        public decimal? Lon { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Worknrby_WebAPI
{
    public class DropDowns
    {

        public IQueryable<TextValue> Gender { get; set; }

        public IQueryable<TextValue> EduLevel { get; set; }

        public IQueryable<TextValue> Industry { get; set; }

        public IQueryable<TextValue> JobRole { get; set; }

        public IQueryable<TextValue> NoOfEmployee { get; set; }

        public IQueryable<TextValue> Skill { get; set; }

        public IQueryable<TextValue> SpamDiscoveryType { get; set; }

        public IQueryable<TextValue> SpamType { get; set; }

        public IQueryable<TextValue> YearsOfExp { get; set; }

        public IQueryable<TextValue> JobType { get; set; }

        public IQueryable<TextValue> NoOfEmployees { get; set; }
    }
}

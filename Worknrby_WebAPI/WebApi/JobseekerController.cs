﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Worknrby_WebAPI.App_Data;

namespace Worknrby_WebAPI
{
    public class JobseekerController : ApiController
    {
        private readonly WnbNewEntities _context;

        public JobseekerController() { _context = new WnbNewEntities(); }

        [HttpPost]
        public IHttpActionResult GetEmployee(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                var data = _context.usp_GetEmployee(UserID).FirstOrDefault();
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult CreateCV(string AccessToken, Employee model, string Mobile = "", string MobileVerificationCode = "")
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                long EmployeeID = 0;
                bool IsNew = false;
                var Employee = _context.Employees.FirstOrDefault(m => m.UserID == UserID);
                if (Employee == null)
                {
                    model.Mobile = Mobile;
                    model.MobileVerificationCode = MobileVerificationCode;
                    model.UserID = Global.GetUserID(_context, AccessToken);
                    model.RadiusInKM = Global.RadiusInKM;
                    Global.ErrorFreeObject(model);
                    _context.Employees.Add(model);
                    IsNew = true;
                }
                else
                {
                    Employee.Name = model.Name;
                    Employee.DOB = model.DOB;
                    Employee.EduLevelID = model.EduLevelID;
                    Employee.ExpectedSalaryMonthly = model.ExpectedSalaryMonthly;
                    Employee.GenderID = model.GenderID;
                    Employee.JobTypeID = model.JobTypeID;
                    Employee.LocID = model.LocID;
                    Employee.Qualification = model.Qualification;
                    Employee.Remarks = model.Remarks;
                    Employee.YrsOfExperience = model.YrsOfExperience;
                    Global.ErrorFreeObject(Employee);
                }

                try
                {
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                if (IsNew)
                    EmployeeID = model.ID;
                else
                    EmployeeID = Employee.ID;

                var lstSkills = model.Skills;
                var lstSkillRatings = model.SkillRatings;

                //Insert/Update Skills
                var ExistingSkills = _context.EmployeeSkills.Where(m => m.EmployeeID == EmployeeID);
                List<long> skills = lstSkills.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList().Select(m => Convert.ToInt64(m)).ToList();
                List<int> skillRatings = lstSkillRatings.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList().Select(m => Convert.ToInt32(m)).ToList();
                var lstRemainingSkills = skills.ToList();
                foreach (var item in ExistingSkills)
                {
                    var Index = skills.IndexOf(item.SkillID);
                    if (Index > -1)
                    {
                        lstRemainingSkills.Remove(item.SkillID);
                        var Rating = skillRatings[Index];
                        if (Rating != item.Level1to5)
                        {
                            item.Level1to5 = Rating;
                        }
                    }
                    else
                    {
                        _context.EmployeeSkills.Remove(item);
                    }
                }
                for (int i = 0; i < lstRemainingSkills.Count; i++)
                {
                    var Index = skills.IndexOf(lstRemainingSkills[i]);
                    EmployeeSkill objES = new EmployeeSkill();
                    objES.EmployeeID = EmployeeID;
                    objES.SkillID = skills[Index];
                    objES.Level1to5 = skillRatings[Index];
                    Global.ErrorFreeObject(objES);
                    _context.EmployeeSkills.Add(objES);
                }
                _context.SaveChanges();
                return Ok(Global.GetAccessToken(UserID, (int)EmployeeID));//Send AccessToken
            }
        }

        [HttpPost]
        public IHttpActionResult SearchJobs(string AccessToken, string Lat = "", string Lon = "", int LocID = 0, bool WithSkill = false, int RadiusInKM = 5)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int EmployeeID = 0;
                if (WithSkill)
                    EmployeeID = Global.GetEmployeeID(_context, AccessToken);
                if (LocID == 0)
                    LocID = (int)_context.Employees.FirstOrDefault(m => m.ID == EmployeeID).LocID;
                var data = _context.usp_SearchJobs(Lat, Lon, RadiusInKM, LocID, WithSkill, EmployeeID).ToList();
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult GetJobDetail(string AccessToken, int JobOpeningID)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                int EmployeeID = Global.GetEmployeeID(_context, AccessToken);
                var JobOpenining = _context.usp_GetJobOpeningDetails(UserID, EmployeeID, JobOpeningID).FirstOrDefault();
                return Ok(JobOpenining);
            }
        }

        [HttpPost]
        public IHttpActionResult Apply(string AccessToken, int JobOpeningID, int JobApplicationDirectionID)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int EmployeeID = Global.GetEmployeeID(_context, AccessToken);
                var obj = _context.JobApplications.FirstOrDefault(m => m.JobOpeningID == JobOpeningID && m.EmployeeID == EmployeeID);
                if (obj != null)
                {
                    obj.JobApplicationDirectionID = JobApplicationDirectionID;
                }
                else
                {
                    JobApplication objJA = new JobApplication();
                    objJA.JobApplicationDirectionID = JobApplicationDirectionID;
                    objJA.JobOpeningID = JobOpeningID;
                    objJA.EmployeeID = EmployeeID;
                    Global.ErrorFreeObject(objJA);
                    _context.JobApplications.Add(objJA);
                }
                _context.SaveChanges();

                return Ok(JobApplicationDirectionID);
            }
        }

        [HttpPost]
        public IHttpActionResult Match(string AccessToken, int JobOpeningID)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                bool Status = false;
                int EmployeeID = Global.GetEmployeeID(_context, AccessToken);
                var objEmployeeWishlists = _context.EmployeeWishlists.FirstOrDefault(m => m.JobOpeningID == JobOpeningID && m.EmployeeID == EmployeeID);
                if (objEmployeeWishlists != null)
                {
                    _context.EmployeeWishlists.Remove(objEmployeeWishlists);
                }
                else
                {
                    Status = true;
                    objEmployeeWishlists = new EmployeeWishlist();
                    objEmployeeWishlists.EmployeeID = EmployeeID;
                    objEmployeeWishlists.JobOpeningID = JobOpeningID;
                    Global.ErrorFreeObject(objEmployeeWishlists);
                    _context.EmployeeWishlists.Add(objEmployeeWishlists);
                }
                _context.SaveChanges();
                return Ok(Status);
            }
        }

        [HttpPost]
        public IHttpActionResult Activate(string AccessToken, bool GetOnly = false)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                try
                {
                    int UserID = Global.GetUserID(_context, AccessToken);
                    var Employee = _context.Employees.FirstOrDefault(m => m.UserID == UserID);
                    if (!GetOnly)
                    {
                        Employee.IsActive = !Employee.IsActive;
                        _context.SaveChanges();
                    }
                    return Ok(Employee.IsActive);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return NotFound();
                }
            }
        }

        [HttpPost]
        public IHttpActionResult Flag(string AccessToken, SpamReport model)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int EmployeeID = Global.GetEmployeeID(_context, AccessToken);
                var objJobViews = _context.SpamReports.FirstOrDefault(m => m.JobOpeningID == model.JobOpeningID && m.ByEmployeeID == EmployeeID);
                if (objJobViews == null)
                {
                    model.ByEmployeeID = EmployeeID;
                    Global.ErrorFreeObject(model);
                    _context.SpamReports.Add(model);
                    _context.SaveChanges();
                }
                return Ok(true);
            }
        }

        [HttpPost]
        public IHttpActionResult GetWishlist(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int EmployeeID = Global.GetEmployeeID(_context, AccessToken);
                var data = _context.usp_GetEmployeeWishlist(EmployeeID).ToList();
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult GetEmployeeMatches(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int EmployeeID = Global.GetEmployeeID(_context, AccessToken);
                var data = _context.usp_GetEmployeeMatches(EmployeeID).ToList();
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult GetProfile(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                var data = (from u in _context.Users
                            join e in _context.Employees on u.ID equals e.UserID
                            join l in _context.Locs on e.LocID equals l.ID
                            where u.ID == UserID
                            select new UserProfile
                            {
                                Email = u.UserName,
                                Name = e.Name,
                                DOB = e.DOB,
                                Mobile = e.Mobile,
                                RadiusInKM = e.RadiusInKM,
                                PinCode = l.Pincode,
                                Location = l.Location,
                                LocID = l.ID
                            }).FirstOrDefault();
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateProfile(string AccessToken, UserProfile Profile)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                int EmployeeID = Global.GetEmployeeID(_context, AccessToken);

                var ObjEmployee = _context.Employees.FirstOrDefault(m => m.ID == EmployeeID);
                ObjEmployee.Name = Profile.Name;
                ObjEmployee.DOB = Profile.DOB;
                ObjEmployee.Mobile = Profile.Mobile;
                ObjEmployee.RadiusInKM = Profile.RadiusInKM;
                ObjEmployee.LocID = Profile.LocID;
                Global.ErrorFreeObject(ObjEmployee);
                _context.SaveChanges();

                return Ok("Success");
            }
        }
    }
}

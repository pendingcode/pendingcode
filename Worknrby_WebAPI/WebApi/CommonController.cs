﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Worknrby_WebAPI.App_Data;


namespace Worknrby_WebAPI
{
    public class CommonController : ApiController
    {
        private readonly WnbNewEntities _context;

        public CommonController() { _context = new WnbNewEntities(); }

        [HttpGet]
        public DropDowns GetAllDropdowns(string AccessToken)
        {
            var EduLevel = _context.EduLevels.Where(m => m.ID != -1).Select(m => new TextValue { Id = m.ID, Value = m.Name });
            var Gender = _context.Genders.Where(m => m.ID != -1).OrderByDescending(m => m.ID).Select(m => new TextValue { Id = m.ID, Value = m.Name });
            var Industry = _context.Industries.Where(m => m.ID != -1).Select(m => new TextValue { Id = m.ID, Value = m.Name });
            var JobRole = _context.JobRoles.Where(m => m.ID != -1).Select(m => new TextValue { Id = m.ID, Value = m.Name });
            var JobType = _context.JobTypes.Where(m => m.ID != -1).Select(m => new TextValue { Id = m.ID, Value = m.Name });
            var NoOfEmployees = _context.NoOfEmployees.Where(m => m.ID != -1).Select(m => new TextValue { Id = m.ID, Value = m.Name });
            var Skill = _context.Skills.Where(m => m.ID != -1).Select(m => new TextValue { Id = m.ID, Value = m.Name });
            var SpamDiscoveryType = _context.SpamDiscoveryTypes.Where(m => m.ID != -1).Select(m => new TextValue { Id = m.ID, Value = m.Name });
            var SpamType = _context.SpamTypes.Where(m => m.ID != -1).Select(m => new TextValue { Id = m.ID, Value = m.Name });
            var YearsOfExp = _context.YearsOfExps.Where(m => m.ID != -1).Select(m => new TextValue { Id = m.ID, Value = m.Name });

            DropDowns objDD = new DropDowns();
            objDD.EduLevel = EduLevel;
            objDD.Gender = Gender;
            objDD.Industry = Industry;
            objDD.JobRole = JobRole;
            objDD.JobType = JobType;
            objDD.NoOfEmployees = NoOfEmployees;
            objDD.Skill = Skill;
            objDD.SpamDiscoveryType = SpamDiscoveryType;
            objDD.SpamType = SpamType;
            objDD.YearsOfExp = YearsOfExp;

            return objDD;
        }

        [HttpPost]
        public IHttpActionResult SocialMediaLogin(string AccessToken, string Name, string Email, string LoginFor, string DeviceID = "")
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                var dbUser = _context.Users.FirstOrDefault(m => m.UserName == Email && m.Role == LoginFor);
                bool IsNew = false;
                int RadiusInKM = Global.RadiusInKM;
                if (dbUser == null)
                {
                    try
                    {
                        dbUser = new User();
                        dbUser.UserName = Email;
                        dbUser.Role = LoginFor;
                        dbUser.OrgID = 1;
                        _context.Users.Add(dbUser);
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        dbUser = new User();
                        dbUser.ID = -1;
                    }
                    IsNew = true;
                }

                if (dbUser.ID > 0 && !string.IsNullOrEmpty(DeviceID))
                {
                    var gcmUser = _context.GcmUsers.FirstOrDefault(m => m.UserID == dbUser.ID && m.RegIDNo == DeviceID);
                    if (gcmUser == null)
                    {
                        gcmUser = new GcmUser();
                        gcmUser.UserID = dbUser.ID;
                        gcmUser.RegIDNo = DeviceID;
                        Global.ErrorFreeObject(gcmUser);
                        _context.GcmUsers.Add(gcmUser);
                        _context.SaveChanges();
                    }
                }

                if (!IsNew)
                {
                    if (LoginFor == "Employee")
                    {
                        var Employee = _context.Employees.FirstOrDefault(m => m.UserID == dbUser.ID);
                        if (Employee != null)
                        {
                            return Ok(new { AccessToken = Global.GetAccessToken((int)dbUser.ID, (int)Employee.ID), IsNew = IsNew, RadiusInKM = Employee.RadiusInKM });
                        }
                    }
                    else
                    {
                        var Employer = _context.Employers.FirstOrDefault(m => m.UserID == dbUser.ID);
                        if (Employer != null)
                        {
                            return Ok(new { AccessToken = Global.GetAccessToken((int)dbUser.ID, (int)Employer.ID), IsNew = IsNew, RadiusInKM = Employer.RadiusInKM });
                        }
                    }
                }

                try
                {
                    if (Request.GetRequestContext().Url.Request.Headers.Referrer.LocalPath.Contains("employer/location.html"))
                    {
                        //Add employer
                    }
                }
                catch
                {
                }

                //Save user in db if not exists
                //Get user if exists
                //Create GUID and save in new table to track user.
                dynamic Response = new { AccessToken = dbUser.ID, IsNew = true, RadiusInKM = RadiusInKM };
                return Ok(Response);//Send AccessToken
            }
        }

        [HttpPost]
        public IHttpActionResult SendVerificationCode(string AccessToken, string MobileNo)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                var VerificationCode = (new Random()).Next(100000, 999999);
                
                if (Global.SendMessage(MobileNo, VerificationCode + " is your mobile verification code for WorkNrBy"))
                    return Ok(VerificationCode);//Send AccessToken
                else
                    return BadRequest();
            }
        }

        [HttpPost]
        public IHttpActionResult Logout(string AccessToken, string DeviceID = "")
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                var UserID = Global.GetUserID(_context, AccessToken);
                if (UserID != 0 && !string.IsNullOrEmpty(DeviceID))
                {
                    var gcmUser = _context.GcmUsers.FirstOrDefault(m => m.UserID == UserID && m.RegIDNo == DeviceID);
                    if (gcmUser != null)
                    {
                        _context.GcmUsers.Remove(gcmUser);
                        _context.SaveChanges();
                    }
                }
                return Ok("Success");
            }
        }
    }
}

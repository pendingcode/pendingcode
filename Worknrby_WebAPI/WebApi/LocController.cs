﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Worknrby_WebAPI.App_Data;

namespace Worknrby_WebAPI
{
    public class LocController : ApiController
    {
        private readonly WnbNewEntities _context;

        public LocController() { _context = new WnbNewEntities(); }


        public dynamic Get(string query)
        {
            var data = from l in _context.Locs
                       //where (l.Pincode.ToLower().Contains(query) || l.Location.ToLower().Contains(query)) && l.Location != null && l.Lat != null && l.Lon != null
                       where l.Pincode == query
                       select new LatLongLocation
                       {
                           id = l.ID,
                           name = l.Pincode + " (" + l.Location + ")",
                           Lat = l.Lat,
                           Lon = l.Lon
                       };
            return data.Take(10).ToList();
        }

        [HttpPost]
        public IHttpActionResult GetPinCodeData(string PinCode)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                var data = from l in _context.Locs
                           where l.Pincode == PinCode
                           select new LatLongLocation
                           {
                               id = l.ID,
                               name = l.Location,
                               Lat = l.Lat,
                               Lon = l.Lon
                           };
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult GetPinCodeData(double Lat, double Long)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                var data = _context.usp_SearchLocViaLatLong(Lat, Long);
                return Ok(data.FirstOrDefault());
            }
        }

    }
}

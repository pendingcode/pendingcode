﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Worknrby_WebAPI.App_Data;

namespace Worknrby_WebAPI
{
    public class EmployerController : ApiController
    {
        private readonly WnbNewEntities _context;

        public EmployerController() { _context = new WnbNewEntities(); }

        [HttpPost]
        public IHttpActionResult JobOpening(string AccessToken, JobOpening model, string Mobile = "", string MobileVerificationCode = "", int LocID = 0, string CompanyName = "")
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                try
                {
                    int UserID = Global.GetUserID(_context, AccessToken);
                    long EmployerID = Global.GetEmployerID(_context, AccessToken);
                    model.CreatedByUserID = UserID;
                    Global.ErrorFreeObject(model);
                    if (model.ID > 0)
                    {
                        var JO = _context.JobOpenings.FirstOrDefault(m => m.ID == model.ID);
                        if (JO != null)
                        {
                            JO.EmployerBranchID = model.EmployerBranchID;
                            JO.Gender = model.Gender;
                            JO.IsNegotiable = model.IsNegotiable;
                            JO.JobRoleID = model.JobRoleID;
                            JO.JobTitle = model.JobTitle;
                            JO.JobTypeID = model.JobTypeID;
                            JO.MinSalaryMonthly = model.MinSalaryMonthly;
                            JO.PrefernceGenderID = model.PrefernceGenderID;
                            JO.Remarks = model.Remarks;
                            JO.YearsOfExpID = model.YearsOfExpID;
                        }
                    }
                    else
                        _context.JobOpenings.Add(model);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                var lstSkills = model.Skills;
                var lstSkillRatings = model.SkillRatings;

                //Insert/Update Skills
                var ExistingSkills = _context.JobOpeningSkills.Where(m => m.JobOpeningID == model.ID);
                List<long> skills = lstSkills.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList().Select(m => Convert.ToInt64(m)).ToList();
                List<int> skillRatings = lstSkillRatings.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList().Select(m => Convert.ToInt32(m)).ToList();
                var lstRemainingSkills = skills.ToList();
                foreach (var item in ExistingSkills)
                {
                    var Index = skills.IndexOf(item.SkillID);
                    if (Index > -1)
                    {
                        lstRemainingSkills.Remove(item.SkillID);
                        var Rating = skillRatings[Index];
                        if (Rating != item.Level1to5)
                        {
                            item.Level1to5 = Rating;
                        }
                    }
                    else
                    {
                        _context.JobOpeningSkills.Remove(item);
                    }
                }
                for (int i = 0; i < lstRemainingSkills.Count; i++)
                {
                    var Index = skills.IndexOf(lstRemainingSkills[i]);
                    JobOpeningSkill objJOS = new JobOpeningSkill();
                    objJOS.JobOpeningID = model.ID;
                    objJOS.SkillID = skills[Index];
                    objJOS.Level1to5 = skillRatings[Index];
                    Global.ErrorFreeObject(objJOS);
                    _context.JobOpeningSkills.Add(objJOS);
                }
                _context.SaveChanges();

                return Ok(model.ID);//Send AccessToken
            }
        }

        [HttpPost]
        public IHttpActionResult SaveEmployer(string AccessToken, string Mobile = "", string MobileVerificationCode = "", int LocID = 0, string CompanyName = "", string Address = "")
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                long EmployerID = Global.GetEmployerID(_context, AccessToken);
                try
                {
                    if (EmployerID == 0)
                    {
                        Employer objEmployer = new Employer();
                        objEmployer.UserID = UserID;
                        objEmployer.CompanyName = CompanyName;
                        objEmployer.Mobile = Mobile;
                        objEmployer.MobileVerificationCode = MobileVerificationCode;
                        objEmployer.RadiusInKM = Global.RadiusInKM;
                        objEmployer.MobileVerified = true;
                        Global.ErrorFreeObject(objEmployer);
                        _context.Employers.Add(objEmployer);
                        _context.SaveChanges();
                        EmployerID = objEmployer.ID;

                        EmployerBranch branch = new EmployerBranch();
                        branch.Address = Address;
                        branch.EmployerID = EmployerID;
                        branch.LocID = LocID;
                        Global.ErrorFreeObject(branch);
                        _context.EmployerBranches.Add(branch);
                        _context.SaveChanges();
                    }
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return Ok(Global.GetAccessToken(UserID, (int)EmployerID));//Send AccessToken
            }
        }

        [HttpPost]
        public IHttpActionResult EmployerBranch(string AccessToken, EmployerBranch model)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                long EmployerID = Global.GetEmployerID(_context, AccessToken);
                model.EmployerID = EmployerID;
                model.CreatedByUserID = UserID;
                Global.ErrorFreeObject(model);
                _context.EmployerBranches.Add(model);
                _context.SaveChanges();
                var Loc = _context.Locs.FirstOrDefault(m => m.ID == model.LocID);
                return Ok(new TextValue { Id = model.ID, Value = Loc.Location });
            }
        }

        [HttpPost]
        public IHttpActionResult SearchJobseekers(string AccessToken, int JobOpeningID = 0, string Lat = "", string Lon = "", int LocID = 0, bool WithSkill = false, int RadiusInKM = 5)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int EmployerID = 0;
                if (WithSkill)
                    EmployerID = Global.GetEmployerID(_context, AccessToken);
                if (LocID == 0)
                    LocID = (int)_context.JobOpenings.FirstOrDefault(m => JobOpeningID != 0 ? m.ID == JobOpeningID : m.EmployerBranch.EmployerID == EmployerID).EmployerBranch.LocID;
                var data = _context.usp_SearchJobseekers(Lat, Lon, RadiusInKM, LocID, WithSkill, JobOpeningID).ToList();
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult GetJobseekerDetail(string AccessToken, int EmployeeID, int JobOpeningID = 0)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                int EmployerID = Global.GetEmployerID(_context, AccessToken);
                if (JobOpeningID == 0)
                    JobOpeningID = Global.GetJobOpeningID(_context, AccessToken);
                var data = _context.usp_GetJobseekerDetails(UserID, EmployeeID, JobOpeningID, EmployerID).FirstOrDefault();
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult GetEmployerBranches(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                int EmployerID = Global.GetEmployerID(_context, AccessToken);
                var data = (from eb in _context.EmployerBranches
                            join l in _context.Locs on eb.LocID equals l.ID
                            where eb.EmployerID == EmployerID
                            select new TextValue
                            {
                                Id = eb.ID,
                                Value = l.Location
                            }).ToList();
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult EmployerJobOpening(string AccessToken, int JobOpeningID)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                var JobOpenining = _context.usp_GetJobOpening(JobOpeningID).FirstOrDefault();
                return Ok(JobOpenining);
            }
        }

        [HttpPost]
        public IHttpActionResult EmployerJobOpenings(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                int EmployeeID = Global.GetEmployeeID(_context, AccessToken);
                var JobOpeninings = _context.usp_GetJobOpenings(UserID, EmployeeID).ToList();
                return Ok(JobOpeninings);
            }
        }

        [HttpPost]
        public IHttpActionResult Apply(string AccessToken, int EmployeeID, int JobApplicationDirectionID, int JobOpeningID = 0)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                if (JobOpeningID == 0)
                    JobOpeningID = Global.GetJobOpeningID(_context, AccessToken);
                var obj = _context.JobApplications.FirstOrDefault(m => m.JobOpeningID == JobOpeningID && m.EmployeeID == EmployeeID);
                if (obj != null)
                {
                    obj.JobApplicationDirectionID = JobApplicationDirectionID;
                }
                else
                {
                    JobApplication objJA = new JobApplication();
                    objJA.JobApplicationDirectionID = JobApplicationDirectionID;
                    objJA.JobOpeningID = JobOpeningID;
                    objJA.EmployeeID = EmployeeID;
                    Global.ErrorFreeObject(objJA);
                    _context.JobApplications.Add(objJA);
                }
                _context.SaveChanges();
                PushNotifications.PushNotificationOnEmployerApply(_context, EmployeeID, JobApplicationDirectionID);
                return Ok(JobApplicationDirectionID);
            }
        }



        [HttpPost]
        public IHttpActionResult Match(string AccessToken, int EmployeeID, int JobOpeningID = 0)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                bool Status = false;
                if (JobOpeningID == 0)
                    JobOpeningID = Global.GetJobOpeningID(_context, AccessToken);
                var objJobOpeningWishlists = _context.JobOpeningWishlists.FirstOrDefault(m => m.JobOpeningID == JobOpeningID && m.EmployeeID == EmployeeID);
                if (objJobOpeningWishlists != null)
                {
                    _context.JobOpeningWishlists.Remove(objJobOpeningWishlists);
                }
                else
                {
                    Status = true;
                    objJobOpeningWishlists = new JobOpeningWishlist();
                    objJobOpeningWishlists.EmployeeID = EmployeeID;
                    objJobOpeningWishlists.JobOpeningID = JobOpeningID;
                    Global.ErrorFreeObject(objJobOpeningWishlists);
                    _context.JobOpeningWishlists.Add(objJobOpeningWishlists);
                }
                _context.SaveChanges();
                return Ok(Status);
            }
        }

        [HttpPost]
        public IHttpActionResult Activate(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                var Employer = _context.Employers.FirstOrDefault(m => m.UserID == UserID);
                Employer.IsActive = !Employer.IsActive;
                _context.SaveChanges();
                return Ok(Employer.IsActive);
            }
        }

        [HttpPost]
        public IHttpActionResult Flag(string AccessToken, SpamReport model)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int EmployerID = Global.GetEmployerID(_context, AccessToken);
                var objJobViews = _context.SpamReports.FirstOrDefault(m => m.EmployeeID == model.EmployeeID && m.ByEmployerID == EmployerID);
                if (objJobViews == null)
                {
                    model.ByEmployerID = EmployerID;
                    Global.ErrorFreeObject(model);
                    _context.SpamReports.Add(model);
                    _context.SaveChanges();
                }
                return Ok(true);
            }
        }

        [HttpPost]
        public IHttpActionResult GetWishlist(string AccessToken, int JobOpeningID = 0)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                if (JobOpeningID == 0)
                    JobOpeningID = Global.GetJobOpeningID(_context, AccessToken);
                var data = _context.usp_GetJobOpeningWishlist(JobOpeningID).ToList();
                return Ok(data);
            }
        }

        [HttpGet]
        public IHttpActionResult AddMoney(string AccessToken, decimal Amount)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                var Employer = _context.Employers.FirstOrDefault(m => m.UserID == UserID);
                var User = _context.Users.FirstOrDefault(m => m.ID == UserID);

                PaymentTransaction pt = new PaymentTransaction();
                pt.OrderTotal = Amount;
                pt.TransDateTime = DateTime.Now;
                pt.TransStatusID = (int)Global.OrderStatus.Pending;
                try
                {
                    pt.TransRemarks = Request.Headers.Referrer.AbsoluteUri.ToString();
                }
                catch
                {
                }
                pt.UserID = UserID;

                Global.ErrorFreeObject(pt);
                _context.PaymentTransactions.Add(pt);
                _context.SaveChanges();

                Random rn = new Random();

                PostProcessPaymentRequest postPreocessPaymentReq = new PostProcessPaymentRequest();
                postPreocessPaymentReq.OrderID = pt.ID;
                postPreocessPaymentReq.OrderTotal = pt.OrderTotal;
                postPreocessPaymentReq.CustomerName = Employer.CompanyName;
                postPreocessPaymentReq.Email = User.UserName;
                postPreocessPaymentReq.Mobile = Employer.Mobile;
                postPreocessPaymentReq.PinCode = "";
                postPreocessPaymentReq.ZipPostalCode = "";
                postPreocessPaymentReq.ReferenceID = "";
                //Go For Payment Gateway..
                PaytmPaymentProcessor.PostProcessPaymentNOP(postPreocessPaymentReq);
                return Ok("Success");
            }
        }

        [HttpPost]
        public IHttpActionResult GetBalance(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                var data = _context.PaymentTransactions.Where(m => m.UserID == UserID && m.TransStatusID == (int)Global.OrderStatus.Success).Sum(m => m.OrderTotal);
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult GetReports(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                try
                {
                    int UserID = Global.GetUserID(_context, AccessToken);

                    List<long> RequestedByEmployerJobApplicationIDs = new List<long> { 2, 3, 5 };
                    List<long> RequestedByEmployeeJobApplicationIDs = new List<long> { 1, 4, 6 };

                    var data = from e in _context.Employers
                               let SuccessPayments = _context.PaymentTransactions.Where(m => m.UserID == e.UserID).OrderByDescending(m => m.ID)
                               let TotalPurchase = SuccessPayments.Count() == 0 ? 0 : SuccessPayments.Where(m => m.TransStatusID == (int)Global.OrderStatus.Success).Select(m => m.OrderTotal).DefaultIfEmpty().Sum()
                               let Expenses = from jo in _context.JobOpenings
                                              join eb in _context.EmployerBranches on jo.EmployerBranchID equals eb.ID
                                              join l in _context.Locs on eb.LocID equals l.ID
                                              where eb.EmployerID == e.ID
                                              orderby jo.ID descending
                                              let RequestedByEmployer = Global.CreditDecutOnEmployerRequest * _context.JobApplications.Where(m => m.JobOpeningID == jo.ID && RequestedByEmployerJobApplicationIDs.Contains(m.JobApplicationDirectionID)).Count()
                                              let RequestedByEmployee = Global.CreditDecutOnEmployeeRequest * _context.JobApplications.Where(m => m.JobOpeningID == jo.ID && RequestedByEmployeeJobApplicationIDs.Contains(m.JobApplicationDirectionID)).Count()
                                              select new
                                              {
                                                  jo.ID,
                                                  jo.JobTitle,
                                                  l.Location,
                                                  RequestedByEmployer,
                                                  RequestedByEmployee
                                              }
                               let TotalSpend = Expenses.Count() == 0 ? 0 : Expenses.Sum(m => m.RequestedByEmployee + m.RequestedByEmployer)
                               let Balance = TotalPurchase - TotalSpend
                               where e.UserID == UserID
                               select new
                               {
                                   TotalPurchase,
                                   SuccessPayments,
                                   Expenses,
                                   TotalSpend,
                                   Balance
                               };
                    return Ok(data.FirstOrDefault());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return NotFound();
                }
            }
        }

        [HttpPost]
        public IHttpActionResult GetJobOpeningMatches(string AccessToken, int JobOpeningID = 0)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                if (JobOpeningID == 0)
                    JobOpeningID = Global.GetJobOpeningID(_context, AccessToken);
                var data = _context.usp_GetJobOpeningMatches(JobOpeningID).ToList();
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult GetProfile(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                var data = (from u in _context.Users
                            join e in _context.Employers on u.ID equals e.UserID
                            where u.ID == UserID
                            select new
                            {
                                Email = u.UserName,
                                e.FirstName,
                                e.LastName,
                                e.Mobile,
                                e.RadiusInKM
                            }).FirstOrDefault();
                return Ok(data);
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateProfile(string AccessToken, Employer Profile)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                int EmployeeID = Global.GetEmployeeID(_context, AccessToken);

                var Employer = _context.Employers.FirstOrDefault(m => m.ID == EmployeeID);
                Employer.FirstName = Profile.FirstName;
                Employer.LastName = Profile.LastName;
                Employer.Mobile = Profile.Mobile;
                Employer.RadiusInKM = Profile.RadiusInKM;
                Global.ErrorFreeObject(Employer);
                _context.SaveChanges();

                return Ok("Success");
            }
        }

        [HttpPost]
        public IHttpActionResult ExportReport(string AccessToken)
        {
            bool valid = true;
            HttpResponseMessage response = new HttpResponseMessage();
            if (!valid)
            {
                return NotFound(); // Returns a NotFoundResult
            }
            else
            {
                int UserID = Global.GetUserID(_context, AccessToken);
                return Ok("Success");
            }
        }
    }
}

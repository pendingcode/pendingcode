﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class PostProcessPaymentRequest
{

    public int OrderID { get; set; }
    public decimal OrderTotal { get; set; }
    public string CustomerName { get; set; }
    public string Email { get; set; }
    public string Mobile { get; set; }
    public string PinCode { get; set; }
    public string ZipPostalCode { get; set; }
    public string ReferenceID { get; set; }
}

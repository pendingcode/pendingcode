﻿using paytm;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Script.Serialization;

public class PaytmPaymentProcessor
{

    #region Methods

    public static void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
    {
        //string amount = postProcessPaymentRequest.Order.OrderTotal.ToString ("#.##");
        //amount.ToString ();
        var remotePostHelper = new RemotePost();
        var remotePostHelperData = new Dictionary<string, string>();

        remotePostHelper.FormName = "PaytmForm";
        remotePostHelper.Url = PaytmPaymentSettings.PaymentUrl;

        //remotePostHelperData.Add("WEBSITE", PaytmPaymentSettings.Website.ToString());
        //remotePostHelperData.Add("CHANNEL_ID", "WEB");
        //remotePostHelperData.Add("INDUSTRY_TYPE_ID", PaytmPaymentSettings.IndustryTypeId.ToString());

        //remotePostHelperData.Add("EMAIL", postProcessPaymentRequest.Email);
        //remotePostHelperData.Add("CUST_ID", postProcessPaymentRequest.CustomerName);
        //remotePostHelperData.Add("CALLBACK_URL", PaytmPaymentSettings.CallBackUrl);


        //remotePostHelperData.Add("MID", PaytmPaymentSettings.MerchantId.ToString());
        //remotePostHelperData.Add("IS_VALIDATION", "T");
        //remotePostHelperData.Add("REQUEST_TYPE", "Add_Money_S2S");
        //remotePostHelperData.Add("PAYMENTMODE", "NEFT");
        //remotePostHelperData.Add("BANK_NAME", PaytmPaymentSettings.BankName);
        //remotePostHelperData.Add("ORDER_ID", postProcessPaymentRequest.OrderID.ToString());
        //remotePostHelperData.Add("TXN_AMOUNT", postProcessPaymentRequest.OrderTotal.ToString("#.##"));
        //remotePostHelperData.Add("TXN_CURRENCY", "INR");
        //remotePostHelperData.Add("MOBILE_NO", postProcessPaymentRequest.Mobile);
        //remotePostHelperData.Add("IS_ALLOW", "0");




        remotePostHelperData.Add("BANK_NAME", PaytmPaymentSettings.BankName);
        remotePostHelperData.Add("MID", PaytmPaymentSettings.MerchantId);
        remotePostHelperData.Add("IS_STATUS_QRY", "F");
        remotePostHelperData.Add("REQUEST_TYPE", "Add_Money_S2S");
        remotePostHelperData.Add("PAYMENTMODE", "NEFT");
        remotePostHelperData.Add("ORDER_ID", Guid.NewGuid().ToString().Substring(0, 25));// postProcessPaymentRequest.OrderID.ToString());
        remotePostHelperData.Add("TXN_AMOUNT", postProcessPaymentRequest.OrderTotal.ToString());
        remotePostHelperData.Add("TXN_CURRENCY", "INR");
        remotePostHelperData.Add("MOBILE_NO", postProcessPaymentRequest.Mobile);
        remotePostHelperData.Add("EMAIL_ID", postProcessPaymentRequest.Email);
        remotePostHelperData.Add("PINCODE", postProcessPaymentRequest.PinCode);
        remotePostHelperData.Add("POS_ID", postProcessPaymentRequest.ZipPostalCode);
        remotePostHelperData.Add("REFERENCE_ID", postProcessPaymentRequest.ReferenceID);
        remotePostHelperData.Add("WEBSITE", "paytm");
        remotePostHelperData.Add("MBID", PaytmPaymentSettings.MBID);

        remotePostHelperData.Add("IS_VALIDATION", "T");
        remotePostHelperData.Add("IS_ALLOW", "0");


        //remotePostHelperData.Add("CALLBACK_URL", _PaytmPaymentSettings.CallBackUrl.ToString());
        Dictionary<string, string> parameters = new Dictionary<string, string>();
        foreach (var item in remotePostHelperData)
        {
            parameters.Add(item.Key, item.Value);
            remotePostHelper.Add(item.Key, item.Value);
        }


        try
        {
            string checksumHash = "";

            checksumHash = CheckSum.generateCheckSum(PaytmPaymentSettings.MerchantKey, parameters);
            remotePostHelper.Add("CHECKSUMHASH", checksumHash);
            remotePostHelper.Post();
        }
        catch (Exception ep)
        {
            throw new Exception(ep.Message);
        }
    }

    public static void PostProcessPaymentNew(PostProcessPaymentRequest postProcessPaymentRequest)
    {
        //var OrderID = "NEFT503001";//Guid.NewGuid().ToString().Replace("-", "").Substring(0, 25);
        //string amount = postProcessPaymentRequest.Order.OrderTotal.ToString ("#.##");
        //amount.ToString ();
        var remotePostHelper = new RemotePost();
        var remotePostHelperData = new Dictionary<string, string>();

        remotePostHelper.FormName = "PaytmForm";
        remotePostHelper.Url = "https://secure.paytm.in/oltp/NEFT?JsonData=";

        remotePostHelperData.Add("BANK_NAME", "PGI");
        remotePostHelperData.Add("ORDER_ID", postProcessPaymentRequest.OrderID.ToString());
        remotePostHelperData.Add("TXN_AMOUNT", "2342.50");
        remotePostHelperData.Add("TXN_CURRENCY", "INR");
        remotePostHelperData.Add("MOBILE_NO", postProcessPaymentRequest.Mobile);
        remotePostHelperData.Add("MID", "P2PTes27664592927446");
        remotePostHelperData.Add("IS_VALIDATION", "T");
        remotePostHelperData.Add("IS_ALLOW", "0");
        remotePostHelperData.Add("REQUEST_TYPE", "Add_Money_S2S");
        remotePostHelperData.Add("PAYMENTMODE", "NEFT");
        //remotePostHelperData.Add("CALLBACK_URL", PaytmPaymentSettings.CallBackUrl);
        //remotePostHelperData.Add("CALLBACK_URL", _PaytmPaymentSettings.CallBackUrl.ToString());

        Dictionary<string, string> parameters = new Dictionary<string, string>();


        foreach (var item in remotePostHelperData)
        {
            parameters.Add(item.Key, item.Value);
            remotePostHelper.Add(item.Key, item.Value);
        }
        try
        {
            string checksumHash = "";

            checksumHash = CheckSum.generateCheckSum(PaytmPaymentSettings.MerchantKey, parameters);
            remotePostHelper.Add("CHECKSUMHASH", checksumHash);

            Boolean success = CheckSum.verifyCheckSum(PaytmPaymentSettings.MerchantKey, parameters, checksumHash);
            Console.WriteLine(success);
            remotePostHelper.Post();
        }
        catch (Exception ep)
        {
            throw new Exception(ep.Message);
        }
    }

    public static void PostProcessPaymentOld(PostProcessPaymentRequest postProcessPaymentRequest)
    {
        //string amount = postProcessPaymentRequest.Order.OrderTotal.ToString ("#.##");
        //amount.ToString ();
        var remotePostHelper = new RemotePost();
        var remotePostHelperData = new Dictionary<string, string>();

        remotePostHelper.FormName = "PaytmForm";
        remotePostHelper.Url = PaytmPaymentSettings.PaymentUrl;
        remotePostHelperData.Add("MID", PaytmPaymentSettings.MerchantId.ToString());
        remotePostHelperData.Add("WEBSITE", "paytm");
        remotePostHelperData.Add("CHANNEL_ID", "WEB");
        remotePostHelperData.Add("INDUSTRY_TYPE_ID", "Retail");
        remotePostHelperData.Add("TXN_AMOUNT", postProcessPaymentRequest.OrderTotal.ToString("#.##"));
        remotePostHelperData.Add("ORDER_ID", postProcessPaymentRequest.OrderID.ToString());
        remotePostHelperData.Add("EMAIL", postProcessPaymentRequest.Email);
        remotePostHelperData.Add("MOBILE_NO", postProcessPaymentRequest.Mobile);
        remotePostHelperData.Add("CUST_ID", postProcessPaymentRequest.Email);
        remotePostHelperData.Add("CALLBACK_URL", PaytmPaymentSettings.CallBackUrl);

        //remotePostHelperData.Add("CALLBACK_URL", _PaytmPaymentSettings.CallBackUrl.ToString());

        Dictionary<string, string> parameters = new Dictionary<string, string>();


        foreach (var item in remotePostHelperData)
        {
            parameters.Add(item.Key, item.Value);
            remotePostHelper.Add(item.Key, item.Value);
        }


        try
        {
            string checksumHash = "";

            checksumHash = CheckSum.generateCheckSum(PaytmPaymentSettings.MerchantKey, parameters);
            remotePostHelper.Add("CHECKSUMHASH", checksumHash);


            remotePostHelper.Post();
        }
        catch (Exception ep)
        {
            throw new Exception(ep.Message);
        }
    }

    public static void PostProcessPaymentNOP(PostProcessPaymentRequest postProcessPaymentRequest)
    {
        //string amount = postProcessPaymentRequest.Order.OrderTotal.ToString ("#.##");
        //amount.ToString ();
        var remotePostHelper = new RemotePost();
        var remotePostHelperData = new Dictionary<string, string>();

        remotePostHelper.FormName = "PaytmForm";
        remotePostHelper.Url = PaytmPaymentSettings.PaymentUrl;
        remotePostHelperData.Add("MID", PaytmPaymentSettings.MerchantId.ToString());
        remotePostHelperData.Add("WEBSITE", PaytmPaymentSettings.Website.ToString());
        remotePostHelperData.Add("CHANNEL_ID", "WEB");
        remotePostHelperData.Add("INDUSTRY_TYPE_ID", PaytmPaymentSettings.IndustryTypeId.ToString());
        remotePostHelperData.Add("TXN_AMOUNT", postProcessPaymentRequest.OrderTotal.ToString("#.##"));
        remotePostHelperData.Add("ORDER_ID", postProcessPaymentRequest.OrderID.ToString());
        remotePostHelperData.Add("EMAIL", postProcessPaymentRequest.Email);
        remotePostHelperData.Add("MOBILE_NO", postProcessPaymentRequest.Mobile);
        remotePostHelperData.Add("CUST_ID", postProcessPaymentRequest.OrderID.ToString());
        remotePostHelperData.Add("CALLBACK_URL", PaytmPaymentSettings.CallBackUrl);

        //remotePostHelperData.Add("CALLBACK_URL", _PaytmPaymentSettings.CallBackUrl.ToString());

        Dictionary<string, string> parameters = new Dictionary<string, string>();


        foreach (var item in remotePostHelperData)
        {
            parameters.Add(item.Key, item.Value);
            remotePostHelper.Add(item.Key, item.Value);
        }


        try
        {
            string checksumHash = "";

            checksumHash = CheckSum.generateCheckSum(PaytmPaymentSettings.MerchantKey, parameters);
            remotePostHelper.Add("CHECKSUMHASH", checksumHash);


            remotePostHelper.Post();
        }
        catch (Exception ep)
        {
            throw new Exception(ep.Message);
        }
    }

    public static void validation_Click()
    {
        Dictionary<string, string> parameters = new Dictionary<string, string>();
        parameters.Add("BANK_NAME", "WALLETTOPUP");
        parameters.Add("ORDER_ID", "orderid18");
        parameters.Add("TXN_AMOUNT", "1");
        parameters.Add("TXN_CURRENCY", "INR");
        parameters.Add("MOBILE_NO", "9252588891"); //7053542171
        parameters.Add("MID", "P2PTes27664592927446");
        parameters.Add("IS_VALIDATION", "T");
        parameters.Add("IS_ALLOW", "0");
        parameters.Add("REQUEST_TYPE", "Add_Money_S2S");
        parameters.Add("PAYMENTMODE", "NEFT");
        string checksum = CheckSum.generateCheckSum("5HP#vfLd7_l&p9A1", parameters);
        parameters.Add("CHECKSUMHASH", checksum);

        string url = "https://pguat.paytm.com/oltp/NEFT?JsonData=";// +new JavaScriptSerializer().Serialize(parameters);
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse resp = (HttpWebResponse)webRequest.GetResponse();
        string responseData = string.Empty;
        using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
        {
            responseData = responseReader.ReadToEnd();
            Console.WriteLine(url + "\n" + responseData);
        }
    }

    public static void txn_Click()
    {
        Dictionary<string, string> parameters = new Dictionary<string, string>();
        Dictionary<string, string> par = new Dictionary<string, string>();
        Dictionary<string, string> param = new Dictionary<string, string>();
        parameters.Add("REQUEST_TYPE", "Add_Money_S2S");
        parameters.Add("PAYMENTMODE", "NEFT");
        parameters.Add("ORDER_ID", "orderid18");
        parameters.Add("TXN_AMOUNT", "1");
        parameters.Add("TXN_CURRENCY", "INR");
        parameters.Add("MOBILE_NO", "9252588891");//9289333444
        parameters.Add("EMAIL_ID", "");
        parameters.Add("PINCODE", "847230");
        parameters.Add("POS_ID", "234567");
        parameters.Add("REFERENCE_ID", "1234");
        parameters.Add("MBID", "WALLETTOPUP");
        parameters.Add("WEBSITE", "paytm");
        //par = parameters;
        parameters.Add("OTP", "623555");
        foreach (string key in parameters.Keys)
        {
            par.Add(key, parameters[key]);
        }

        parameters.Add("BANK_NAME", "WALLETTOPUP");
        parameters.Add("MID", "P2PTes27664592927446");
        parameters.Add("IS_STATUS_QRY", "F");


        string checksum = CheckSum.generateCheckSum("5HP#vfLd7_l&p9A1", parameters);

        par.Add("CHECKSUMHASH", checksum);

        string sa = "";// new JavaScriptSerializer().Serialize(par);

        param.Add("BANK_NAME", "WALLETTOPUP");
        param.Add("MID", "P2PTes27664592927446");
        param.Add("IS_STATUS_QRY", "F");
        param.Add("Detail", sa);

        string url = "https://pguat.paytm.com/oltp/NEFT?JsonData=";// +new JavaScriptSerializer().Serialize(param);
        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse resp = (HttpWebResponse)webRequest.GetResponse();
        string responseData = string.Empty;
        using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
        {
            responseData = responseReader.ReadToEnd();
            Console.Write(responseData + url);
        }
    }

    //public static void ProcessPayment(PostProcessPaymentRequest postPPR)
    //{

    //    //var myUtility = new CheckSum();
    //    //int orderId = postProcessPaymentRequest.OrderID;
    //    //var txnid = "";


    //    //Random rnd = new Random();
    //    //string strHash = myUtility.Generatehash512(rnd.ToString() + DateTime.Now);
    //    //txnid = strHash.ToString().Substring(0, 20) + "_" + orderId;


    //    //var remotePostHelper = new RemotePost();
    //    //remotePostHelper.Url = PaytmPaymentSettings.PayUri;
    //    //remotePostHelper.Add("hash", myUtility.getchecksum(PayuPaymentSettings.Key.ToString(),
    //    //    txnid, postProcessPaymentRequest.OrderTotal.ToString(new CultureInfo("en-US", false).NumberFormat),
    //    //    "productinfo", postProcessPaymentRequest.CustomerName,
    //    //    postProcessPaymentRequest.Email, PayuPaymentSettings.Salt));
    //    //remotePostHelper.Add("txnid", txnid);// orderId.ToString());
    //    //remotePostHelper.Add("key", PayuPaymentSettings.Key.ToString());
    //    //remotePostHelper.Add("amount", postProcessPaymentRequest.OrderTotal.ToString(new CultureInfo("en-US", false).NumberFormat));
    //    //if (customer != null)
    //    //{
    //    //    //basic details 
    //    //    remotePostHelper.Add("firstname", customer.FirstName.ToString());
    //    //    remotePostHelper.Add("lastname", customer.LastName.ToString());

    //    //    remotePostHelper.Add("email", customer.Email.ToString());
    //    //    remotePostHelper.Add("phone", customer.Phone);
    //    //    remotePostHelper.Add("productinfo", "productinfo");
    //    //    remotePostHelper.Add("surl", Global.WebSiteRootPath + "/PaymentPayu/Return");
    //    //    remotePostHelper.Add("furl", Global.WebSiteRootPath + "/PaymentPayu/Return");
    //    //    // Billing Address Details
    //    //    if (!string.IsNullOrEmpty(customer.Address))
    //    //        remotePostHelper.Add("address1", customer.Address);
    //    //    if (!string.IsNullOrEmpty(customer.City))
    //    //        remotePostHelper.Add("city", customer.City);
    //    //    if (!string.IsNullOrEmpty(customer.State))
    //    //        remotePostHelper.Add("state", customer.State);
    //    //    if (!string.IsNullOrEmpty(customer.PostalCode))
    //    //        remotePostHelper.Add("zipcode", customer.PostalCode);

    //    //    remotePostHelper.Add("country", "india");
    //    //    if (objShippingAddress != null) // Shipping Address Details
    //    //    {
    //    //        remotePostHelper.Add("shipping_firstname", objShippingAddress.FirstName);
    //    //        remotePostHelper.Add("shipping_lastname", objShippingAddress.LastName);
    //    //        remotePostHelper.Add("shipping_address1", objShippingAddress.Address);
    //    //        remotePostHelper.Add("shipping_city", objShippingAddress.City);
    //    //        remotePostHelper.Add("shipping_state", objShippingAddress.State);
    //    //        remotePostHelper.Add("shipping_country", "india");
    //    //        remotePostHelper.Add("shipping_zipcode", objShippingAddress.PostalCode);
    //    //        remotePostHelper.Add("shipping_phone", objShippingAddress.Phone);
    //    //        remotePostHelper.Add("shipping_phoneverified", "no");
    //    //    }
    //    //}



    //    #region "Param"
    //    RemotePost remotePost = new RemotePost();
    //    remotePost.Url = PaytmPaymentSettings.URL;
    //    remotePost.Add("BANK_NAME", PaytmPaymentSettings.BankName);
    //    remotePost.Add("MID", PaytmPaymentSettings.Mid);
    //    remotePost.Add("MBID", PaytmPaymentSettings.MBID);
    //    remotePost.Add("IS_STATUS_QRY", "F");
    //    remotePost.Add("IS_VALIDATION", "T");
    //    remotePost.Add("IS_ALLOW", "0");
    //    remotePost.Add("TXN_CURRENCY", "INR");
    //    remotePost.Add("WEBSITE", "paytm");
    //    remotePost.Add("REQUEST_TYPE", "Add_Money_S2S");
    //    remotePost.Add("PAYMENTMODE", "NEFT");//use any mode -> FILE, NEFT, CASH_PT, ATM_PT, P2A_IMPS
    //    remotePost.Add("ORDER_ID", postPPR.OrderID.ToString());//order id will auto genereted 
    //    remotePost.Add("TXN_AMOUNT", postPPR.OrderTotal.ToString());//order amount
    //    remotePost.Add("MOBILE_NO", postPPR.Mobile);//customer mobile no
    //    remotePost.Add("EMAIL_ID", postPPR.Email);// custoemer email
    //    remotePost.Add("PINCODE", postPPR.PinCode);
    //    remotePost.Add("POS_ID", postPPR.ZipPostalCode);
    //    remotePost.Add("REFERENCE_ID", postPPR.ReferenceID);
    //    //String masterKey = PaytmPaymentSettings.Key;
    //    //Dictionary<String, String> parameters = new Dictionary<string, string>();
    //    //parameters.Add("CHANNEL_ID", "WEB"); parameters.Add("TXN_AMOUNT", "1");
    //    //String checkSum = CheckSum.generateCheckSum(masterKey, parameters); 
    //    #endregion

    //    remotePost.Post();





    //}


    #endregion

}


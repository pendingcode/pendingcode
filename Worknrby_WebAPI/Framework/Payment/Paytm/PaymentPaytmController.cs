﻿using paytm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Worknrby_WebAPI.App_Data;

public class PaymentPaytmController : Controller
{
    private readonly WnbNewEntities _context;

    public PaymentPaytmController()
    {
        _context = new WnbNewEntities();
    }

    // GET: PaymentPaytm
    public ActionResult Index()
    {
        return View();
    }

    [ValidateInput(false)]
    public ActionResult Return(FormCollection form)
    {
        var myUtility = new PaytmHelper();
        string orderId, Amount, AuthDesc, ResCode;
        bool checkSumMatch = false;
        //Assign following values to send it to verifychecksum function.
        if (String.IsNullOrWhiteSpace(PaytmPaymentSettings.MerchantKey))
            throw new Exception("Paytm key is not set");

        string workingKey = PaytmPaymentSettings.MerchantKey;


        Dictionary<string, string> parameters = new Dictionary<string, string>();
        if (Request.Form.AllKeys.Length > 0)
        {
            string paytmChecksum = "";
            foreach (string key in Request.Form.Keys)
            {
                parameters.Add(key.Trim(), Request.Form[key].Trim());
            }

            if (parameters.ContainsKey("CHECKSUMHASH"))
            {
                paytmChecksum = parameters["CHECKSUMHASH"];
                parameters.Remove("CHECKSUMHASH");
            }

            if (CheckSum.verifyCheckSum(workingKey, parameters, paytmChecksum))
            {
                checkSumMatch = true;
            }
        }

        orderId = parameters["ORDERID"];
        Amount = parameters["TXNAMOUNT"];
        ResCode = parameters["RESPCODE"];
        AuthDesc = parameters["STATUS"];

        if (ResCode == "01")
        {
            if (checkSumMatch == true)
            {
                var PaymentTransactionsID = Convert.ToInt32(orderId);
                var Order = _context.PaymentTransactions.FirstOrDefault(m => m.ID == PaymentTransactionsID);
                if (AuthDesc == "TXN_SUCCESS")
                {
                    Order.TransStatusID = (int)Worknrby_WebAPI.Global.OrderStatus.Success;
                    _context.SaveChanges();
                    if (!string.IsNullOrEmpty(Order.TransRemarks))
                    {
                        return Redirect(Order.TransRemarks.Replace("-credit", "_chart"));
                    }
                    return Content("Payment Success");
                    //order create here
                }
                else if (AuthDesc == "TXN_FAILURE")
                {
                    Order.TransStatusID = (int)Worknrby_WebAPI.Global.OrderStatus.Fail;
                    _context.SaveChanges();
                    return Content("Thank you for shopping with us. However, the transaction has been declined");
                }
                else
                {
                    Order.TransStatusID = (int)Worknrby_WebAPI.Global.OrderStatus.Fail;
                    _context.SaveChanges();
                    return Content("Security Error. Illegal access detected");
                }
            }
            else
            {
                return Content("Security Error. Illegal access detected, Checksum failed");
            }
        }
        else
        {
            return Content("Transaction has been Failed");
        }

        return View();

    }
}
